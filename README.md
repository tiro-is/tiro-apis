# Tiro API Protocol Buffer Files

These are the gRPC protocol buffer definitions for Tiro APIs. 

The Speech API is based on the Google Cloud Speech API and is mostly
compatible. Synchronous, streaming and long running recognition is supported in
v2beta1.

- [Documentation](https://docs.tiro.is)

## Available services

- [Speech](lr/speech/v2beta1/speech.proto)

## How to use

Since these are gRPC PB service definitions you can use simply use `protoc` to
generate the client stub necessary for your programming language. If you are
unfamiliar with how to do so, please refer to the documentation for [Protocol
Buffers](https://developers.google.com/protocol-buffers/docs/overview) and
[gRPC](https://www.grpc.io/docs/).

### Python Package

The generated modules can be installed with

    pip install tiro-apis@git+https://gitlab.com/tiro-is/tiro-apis.git@master


Please see the [documentation](https://docs.tiro.is).

