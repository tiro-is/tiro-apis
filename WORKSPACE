workspace(name = "com_gitlab_tiro_is_tiro_apis")

load("//:repositories.bzl", "repositories", "deploy_repositories")

repositories()


#####################
# Bazel rules for Go
#
load("@io_bazel_rules_go//go:deps.bzl", "go_register_toolchains", "go_rules_dependencies")

go_rules_dependencies()

go_register_toolchains(version = "1.18")


#################################
# Dependencies for doc generation
load("@rules_python//python:repositories.bzl", "python_register_toolchains", "py_repositories")
py_repositories()

python_register_toolchains(
    name = "python3_9",
    # Available versions are listed in @rules_python//python:versions.bzl.
    python_version = "3.9",
)

load("@python3_9//:defs.bzl", "interpreter")
load("@rules_python//python:pip.bzl", "pip_parse")
pip_parse(
    name = "mkdocs_deps",
    requirements_lock = "@//docs:requirements.txt",
    python_interpreter_target = interpreter,
)

load("@mkdocs_deps//:requirements.bzl", install_pip_requirements = "install_deps")
install_pip_requirements()


deploy_repositories()

##########################
# Import gRPC dependencies
load("@com_github_grpc_grpc//bazel:grpc_deps.bzl", "grpc_deps")

grpc_deps()
# grpc_test_only_deps()
load("@com_github_grpc_grpc//bazel:grpc_extra_deps.bzl", "grpc_extra_deps")


load("@com_google_googleapis//:repository_rules.bzl", "switched_rules_by_language")

switched_rules_by_language(
    name = "com_google_googleapis_imports",
    cc = True,
    # csharp = True,
    # gapic = True,
    # go = False,
    grpc = True,
    # java = True,
    # nodejs = True,
    # php = True,
    python = True,
    # ruby = True,
)

########################################
# Missing dependencies of bazel_gazelle
#
# Used to generate BUILD rules from go.mod which is used by some dependencies
load("@bazel_gazelle//:deps.bzl", "gazelle_dependencies")

gazelle_dependencies()

###############################
# Dependecies for grpc-gateway
load(
    "@grpc_ecosystem_grpc_gateway//:repositories.bzl",
    grpc_gateway_go_repos = "go_repositories",
)

grpc_gateway_go_repos()


########################################
# Dependencies for io_bazel_rules_docker
load(
    "@io_bazel_rules_docker//repositories:repositories.bzl",
    container_repositories = "repositories",
)

container_repositories()

load("@io_bazel_rules_docker//repositories:deps.bzl", container_deps = "deps")

container_deps()

load(
    "@io_bazel_rules_docker//container:container.bzl",
    "container_pull",
)

# Base image for our docs
container_pull(
    name = "nginx_x86_64",
    # 1.19.6-alpine amd64
    digest = "sha256:01747306a7247dbe928db991eab42e4002118bf636dd85b4ffea05dd907e5b66",
    registry = "index.docker.io",
    repository = "library/nginx",
)

###########################
# rules_gitops dependencies
load("@com_adobe_rules_gitops//gitops:deps.bzl", "rules_gitops_dependencies")

rules_gitops_dependencies()

load("@com_adobe_rules_gitops//gitops:repositories.bzl", "rules_gitops_repositories")

rules_gitops_repositories()
