# Documentation for Tiro APIs

Tiro offers access to a speech recognition (speech-to-text) service through a
[gRPC API](./reference/grpc_speech_to_text/speech/) and a [REST
API](./rest/speech).

The [Protocol Buffer](https://developers.google.com/protocol-buffers/docs/overview)
definitions to generate client stubs are available at
[https://gitlab.com/tiro-is/tiro-apis](https://gitlab.com/tiro-is/tiro-apis).

If you are unfamiliar with how to generate client stubs for your language,
please refer to the documentation for [Protocol Buffers](https://developers.google.com/protocol-buffers/docs/overview)
and [gRPC](https://www.grpc.io/docs/).

## Authentication

Using Tiro services requires using an authentication token. To request access
please contact [tiro@tiro.is](mailto:tiro@tiro.is).
