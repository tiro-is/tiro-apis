---
title: Speech-to-Text REST API
---

# Speech REST API Reference

This document is a work in progress. Meanwhile, see [gRPC
transcoding](/reference/rest/http/#HttpRule) and consult the gRPC API reference.


## Authorization

Calls to the Tiro speech-to-text service need to be authorized with an API key
or a JWT token signed with the client's private key. The key or token is either
supplied in an HTTP header `Authorization: Bearer ACCESS_TOKEN` or as a
query parameter `token=ACCESS_TOKEN`.

An API key or client private key can be generated in the API console. Contact
[tiro@tiro.is](mailto:tiro@tiro.is) to gain access or request an access token.

## Server host

`https://speech.talgreinir.is`

## Objects

### RecognitionConfig

```
{
  "encoding": "LINEAR16",
  "sampleRateHertz": 8000-48000,
  "enableWordTimeOffsets: boolean,
  "maxAlternatives: 1-10,
  "languageCode": "is-IS" | "en-US",
}
```

### WordInfo

```
{
  "startTime": "<float>s",
  "endTime": "<float>s",
  "word": string
}
```

## Recognize ─ Speech-to-Text of Short Segments

Synchronous recognition of short segments (less than 30s) of audio.

> POST /v2beta1/speech:recognize

### Body

```
{
 "config": RecognitionConfig,
 "audio": {
   "content": base64 encoded PCM LINEAR 16 audio
 }
}
```

### Response

```
{
  "results": [
  {
    "alternatives": [             # Array of length 1 to config.maxAlternatives
      {
        "transcript": string,     # Recognized text
        "words": WordInfo[]       # Array of WordInfos for each word in the transcript.
                                  # Included if config.enableWordTimeOffset was true
      },
      {
        "transcript": string
      }
    ]
}
```

### Example

Example using `curl`:

```sh
echo "{\"config\": {\"languageCode\": \"is-IS\",\"encoding\": \"LINEAR16\", \"sampleRateHertz\": 16000,  \"enableWordTimeOffsets\": true},  \"audio\" : {\"content\": \"$(sox speech.wav -r16k -t raw - | base64 -w0)\"}}" \
  | curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $ACCESS_TOKEN" \
      -d@- https://speech.talgreinir.is/v2beta1/speech:recognize | jq
```

Example response:


```json
{
  "results": [
    {
      "alternatives": [
        {
          "transcript": "undir þessa rekstrareiningu fellur allur verslunarrekstur á Íslandi með mat og sérvöru auk reksturs í Svíþjóð vegna Bónuss og Hagkaups ",
          "words": [
            {
              "startTime": "0.480s",
              "endTime": "0.900s",
              "word": "undir"
            },
            {
              "startTime": "0.899s",
              "endTime": "1.259s",
              "word": "þessa"
            },
            {
              "startTime": "1.260s",
              "endTime": "2.159s",
              "word": "rekstrareiningu"
            },
            {
              "startTime": "2.310s",
              "endTime": "2.730s",
              "word": "fellur"
            },
            {
              "startTime": "2.729s",
              "endTime": "3.089s",
              "word": "allur"
            },
            {
              "startTime": "3.090s",
              "endTime": "3.930s",
              "word": "verslunarrekstur"
            },
            {
              "startTime": "3.930s",
              "endTime": "4.020s",
              "word": "á"
            },
            {
              "startTime": "4.019s",
              "endTime": "4.679s",
              "word": "Íslandi"
            },
            {
              "startTime": "4.709s",
              "endTime": "4.978s",
              "word": "með"
            },
            {
              "startTime": "4.980s",
              "endTime": "5.400s",
              "word": "mat"
            },
            {
              "startTime": "5.400s",
              "endTime": "5.549s",
              "word": "og"
            },
            {
              "startTime": "5.549s",
              "endTime": "6.179s",
              "word": "sérvöru"
            },
            {
              "startTime": "7.020s",
              "endTime": "7.350s",
              "word": "auk"
            },
            {
              "startTime": "7.350s",
              "endTime": "7.889s",
              "word": "reksturs"
            },
            {
              "startTime": "7.889s",
              "endTime": "7.979s",
              "word": "í"
            },
            {
              "startTime": "7.979s",
              "endTime": "8.668s",
              "word": "Svíþjóð"
            },
            {
              "startTime": "8.729s",
              "endTime": "9.059s",
              "word": "vegna"
            },
            {
              "startTime": "9.060s",
              "endTime": "9.659s",
              "word": "Bónuss"
            },
            {
              "startTime": "9.690s",
              "endTime": "9.930s",
              "word": "og"
            },
            {
              "startTime": "9.930s",
              "endTime": "10.590s",
              "word": "Hagkaups"
            }
          ]
        }
      ]
    }
  ]
}
```


## StreamingRecognize ─ Bidirectional Real-Time Speech-to-Text

Bidirectional streaming of requests and responses using WebSockets. Continuously
accepts chunks of audio emitting responses as soon as they are ready.


WebSocket endpoint:
> /v2beta1/speech:streamingrecognize?token=ACCESS_TOKEN

The access token can also be supplied in the `Sec-Websocket-Protocol` field:

> Sec-Websocket-Protocol: Bearer, ACCESS_TOKEN

*Note: You can not use an Authorization header with WebSockets*

The first message sent in a session *must* be a configuration message:

```json
{
  "streamingConfig": {
    "config": RecognitionConfig,
    "interimResults": boolean,   # Whether to enable non-final responses
    "singleUtterance": boolean   # Whether finalize and close session after first detected endpoint
  }
}
```

Subsequent messages *must* be in the following format:


```json
{
  "audioContent": string        # base64 encoded PCM LINEAR 16 or empty string to finalize session
}
```

The responses have the following format:

```json
{
  "result":
  {
    "results": [
    {
      "alternatives": [
        {
          "transcript": string,       # Recognize text for a segment
          "words": WordInfo[]         # Array of WordInfos for each word in the transcript.
                                      # Included if config.enableWordTimeOffset was true.
        },
        ...
      ],
      "isFinal": boolean              # True if this is the final response emitted for this segment
                                      # of audio, i.e. from the startTime of the first WordInfo to
                                      # endTime of the last WordInfo.
      }
    ]
  }
}
```

The service will emit multiple responses for each segment if
`streamingConfig.interimResults=true`, or everytime the segment hypothesis has
an update. The service will emit a message where `isFinal=true` everytime a
speech endpoint is detected, which usually corresponds to a period of silence.


### Example message exchange

Acquire an access token and connect to
`wss://speech.talgreinir.is/v2beta1/speech:streamingrecognize?token=ACCESS_TOKEN`.

Initial request message:

```json
{
  "streamingConfig": {
    "config": {
      "encoding": "LINEAR16",
      "sampleRateHertz": 48000,
      "enableWordTimeOffsets: true,
      "languageCode": "is-IS",
    },
    "interimResults": true,
  }
}
```

Responses received while sending a LINEAR 16 PCM stream in 800 sample chunks:

```json
{
  "result": {
    "results": [
      {
        "alternatives": [
          {
            "transcript": "undir ",
            "words": [
              {
                "startTime": "0.496s",
                "endTime": "0.856s",
                "word": "undir"
              }
            ]
          }
        ]
      }
    ]
  }
}
{
  "result": {
    "results": [
      {
        "alternatives": [
          {
            "transcript": "undir þessa rekstrareiningu ",
            "words": [
              {
                "startTime": "0.496s",
                "endTime": "0.886s",
                "word": "undir"
              },
              {
                "startTime": "0.886s",
                "endTime": "1.246s",
                "word": "þessa"
              },
              {
                "startTime": "1.246s",
                "endTime": "1.906s",
                "word": "rekstrareiningu"
              }
            ]
          }
        ]
      }
    ]
  }
}
{
  "result": {
    "results": [
      {
        "alternatives": [
          {
            "transcript": "undir þessa rekstrareiningu fellur allur ",
            "words": [
              {
                "startTime": "0.496s",
                "endTime": "0.886s",
                "word": "undir"
              },
              {
                "startTime": "0.886s",
                "endTime": "1.246s",
                "word": "þessa"
              },
              {
                "startTime": "1.246s",
                "endTime": "2.176s",
                "word": "rekstrareiningu"
              },
              {
                "startTime": "2.326s",
                "endTime": "2.746s",
                "word": "fellur"
              },
              {
                "startTime": "2.745s",
                "endTime": "2.955s",
                "word": "allur"
              }
            ]
          }
        ]
      }
    ]
  }
}
{
  "result": {
    "results": [
      {
        "alternatives": [
          {
            "transcript": "undir þessa rekstrareiningu fellur allur verslunarrekstur á ",
            "words": [
              {
                "startTime": "0.496s",
                "endTime": "0.886s",
                "word": "undir"
              },
              {
                "startTime": "0.886s",
                "endTime": "1.246s",
                "word": "þessa"
              },
              {
                "startTime": "1.246s",
                "endTime": "2.176s",
                "word": "rekstrareiningu"
              },
              {
                "startTime": "2.326s",
                "endTime": "2.746s",
                "word": "fellur"
              },
              {
                "startTime": "2.745s",
                "endTime": "3.075s",
                "word": "allur"
              },
              {
                "startTime": "3.106s",
                "endTime": "3.946s",
                "word": "verslunarrekstur"
              },
              {
                "startTime": "3.946s",
                "endTime": "4.006s",
                "word": "á"
              }
            ]
          }
        ]
      }
    ]
  }
}
{
  "result": {
    "results": [
      {
        "alternatives": [
          {
            "transcript": "undir þessa rekstrareiningu fellur allur verslunarrekstur á Íslandi með ",
            "words": [
              {
                "startTime": "0.496s",
                "endTime": "0.886s",
                "word": "undir"
              },
              {
                "startTime": "0.886s",
                "endTime": "1.246s",
                "word": "þessa"
              },
              {
                "startTime": "1.246s",
                "endTime": "2.176s",
                "word": "rekstrareiningu"
              },
              {
                "startTime": "2.326s",
                "endTime": "2.746s",
                "word": "fellur"
              },
              {
                "startTime": "2.745s",
                "endTime": "3.075s",
                "word": "allur"
              },
              {
                "startTime": "3.106s",
                "endTime": "3.946s",
                "word": "verslunarrekstur"
              },
              {
                "startTime": "3.946s",
                "endTime": "4.036s",
                "word": "á"
              },
              {
                "startTime": "4.035s",
                "endTime": "4.665s",
                "word": "Íslandi"
              },
              {
                "startTime": "4.725s",
                "endTime": "4.994s",
                "word": "með"
              }
            ]
          }
        ]
      }
    ]
  }
}
{
  "result": {
    "results": [
      {
        "alternatives": [
          {
            "transcript": "undir þessa rekstrareiningu fellur allur verslunarrekstur á Íslandi með mat og sérvöru ",
            "words": [
              {
                "startTime": "0.496s",
                "endTime": "0.886s",
                "word": "undir"
              },
              {
                "startTime": "0.886s",
                "endTime": "1.246s",
                "word": "þessa"
              },
              {
                "startTime": "1.246s",
                "endTime": "2.176s",
                "word": "rekstrareiningu"
              },
              {
                "startTime": "2.326s",
                "endTime": "2.746s",
                "word": "fellur"
              },
              {
                "startTime": "2.745s",
                "endTime": "3.075s",
                "word": "allur"
              },
              {
                "startTime": "3.106s",
                "endTime": "3.946s",
                "word": "verslunarrekstur"
              },
              {
                "startTime": "3.946s",
                "endTime": "4.036s",
                "word": "á"
              },
              {
                "startTime": "4.035s",
                "endTime": "4.665s",
                "word": "Íslandi"
              },
              {
                "startTime": "4.725s",
                "endTime": "4.994s",
                "word": "með"
              },
              {
                "startTime": "4.996s",
                "endTime": "5.416s",
                "word": "mat"
              },
              {
                "startTime": "5.416s",
                "endTime": "5.565s",
                "word": "og"
              },
              {
                "startTime": "5.565s",
                "endTime": "5.895s",
                "word": "sérvöru"
              }
            ]
          }
        ]
      }
    ]
  }
}
{
  "result": {
    "results": [
      {
        "alternatives": [
          {
            "transcript": "undir þessa rekstrareiningu fellur allur verslunarrekstur á Íslandi með mat og sérvöru ",
            "words": [
              {
                "startTime": "0.496s",
                "endTime": "0.886s",
                "word": "undir"
              },
              {
                "startTime": "0.886s",
                "endTime": "1.246s",
                "word": "þessa"
              },
              {
                "startTime": "1.246s",
                "endTime": "2.176s",
                "word": "rekstrareiningu"
              },
              {
                "startTime": "2.326s",
                "endTime": "2.746s",
                "word": "fellur"
              },
              {
                "startTime": "2.745s",
                "endTime": "3.075s",
                "word": "allur"
              },
              {
                "startTime": "3.106s",
                "endTime": "3.946s",
                "word": "verslunarrekstur"
              },
              {
                "startTime": "3.946s",
                "endTime": "4.036s",
                "word": "á"
              },
              {
                "startTime": "4.035s",
                "endTime": "4.665s",
                "word": "Íslandi"
              },
              {
                "startTime": "4.725s",
                "endTime": "4.994s",
                "word": "með"
              },
              {
                "startTime": "4.996s",
                "endTime": "5.416s",
                "word": "mat"
              },
              {
                "startTime": "5.416s",
                "endTime": "5.565s",
                "word": "og"
              },
              {
                "startTime": "5.565s",
                "endTime": "6.164s",
                "word": "sérvöru"
              }
            ]
          }
        ],
        "isFinal": true
      }
    ]
  }
}
{
  "result": {
    "results": [
      {
        "alternatives": [
          {
            "transcript": "auk rektors ",
            "words": [
              {
                "startTime": "6.957s",
                "endTime": "7.287s",
                "word": "auk"
              },
              {
                "startTime": "7.287s",
                "endTime": "7.527s",
                "word": "rektors"
              }
            ]
          }
        ]
      }
    ]
  }
}
{
  "result": {
    "results": [
      {
        "alternatives": [
          {
            "transcript": "auk reksturs í ",
            "words": [
              {
                "startTime": "6.957s",
                "endTime": "7.287s",
                "word": "auk"
              },
              {
                "startTime": "7.287s",
                "endTime": "7.857s",
                "word": "reksturs"
              },
              {
                "startTime": "7.858s",
                "endTime": "7.948s",
                "word": "í"
              }
            ]
          }
        ]
      }
    ]
  }
}
{
  "result": {
    "results": [
      {
        "alternatives": [
          {
            "transcript": "auk reksturs í Svíþjóð vegna ",
            "words": [
              {
                "startTime": "6.957s",
                "endTime": "7.287s",
                "word": "auk"
              },
              {
                "startTime": "7.287s",
                "endTime": "7.857s",
                "word": "reksturs"
              },
              {
                "startTime": "7.858s",
                "endTime": "7.948s",
                "word": "í"
              },
              {
                "startTime": "7.948s",
                "endTime": "8.637s",
                "word": "Svíþjóð"
              },
              {
                "startTime": "8.697s",
                "endTime": "9.027s",
                "word": "vegna"
              }
            ]
          }
        ]
      }
    ]
  }
}
{
  "result": {
    "results": [
      {
        "alternatives": [
          {
            "transcript": "auk reksturs í Svíþjóð vegna Bónuss og Hagkaups ",
            "words": [
              {
                "startTime": "6.957s",
                "endTime": "7.287s",
                "word": "auk"
              },
              {
                "startTime": "7.287s",
                "endTime": "7.857s",
                "word": "reksturs"
              },
              {
                "startTime": "7.858s",
                "endTime": "7.948s",
                "word": "í"
              },
              {
                "startTime": "7.948s",
                "endTime": "8.637s",
                "word": "Svíþjóð"
              },
              {
                "startTime": "8.697s",
                "endTime": "9.027s",
                "word": "vegna"
              },
              {
                "startTime": "9.028s",
                "endTime": "9.627s",
                "word": "Bónuss"
              },
              {
                "startTime": "9.657s",
                "endTime": "9.867s",
                "word": "og"
              },
              {
                "startTime": "9.868s",
                "endTime": "10.467s",
                "word": "Hagkaups"
              }
            ]
          }
        ]
      }
    ]
  }
}
{
  "result": {
    "results": [
      {
        "alternatives": [
          {
            "transcript": "auk reksturs í Svíþjóð vegna Bónuss og Hagkaups ",
            "words": [
              {
                "startTime": "6.957s",
                "endTime": "7.287s",
                "word": "auk"
              },
              {
                "startTime": "7.287s",
                "endTime": "7.857s",
                "word": "reksturs"
              },
              {
                "startTime": "7.858s",
                "endTime": "7.948s",
                "word": "í"
              },
              {
                "startTime": "7.948s",
                "endTime": "8.637s",
                "word": "Svíþjóð"
              },
              {
                "startTime": "8.697s",
                "endTime": "9.027s",
                "word": "vegna"
              },
              {
                "startTime": "9.028s",
                "endTime": "9.627s",
                "word": "Bónuss"
              },
              {
                "startTime": "9.657s",
                "endTime": "9.867s",
                "word": "og"
              },
              {
                "startTime": "9.868s",
                "endTime": "10.557s",
                "word": "Hagkaups"
              }
            ]
          }
        ],
        "isFinal": true
      }
    ]
  }
}
```
