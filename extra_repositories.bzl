load("@com_google_googleapis//:repository_rules.bzl", "switched_rules_by_language")

def extra_repositories():
    switched_rules_by_language(
        name = "com_google_googleapis_imports",
        cc = True,
        # csharp = True,
        # gapic = True,
        # go = False,
        grpc = True,
        # java = True,
        # nodejs = True,
        # php = True,
        python = True,
        # ruby = True,
    )
