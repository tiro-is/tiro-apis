package(default_visibility = ["//visibility:public"])

load("@rules_proto//proto:defs.bzl", "proto_library")
load("@io_bazel_rules_go//go:def.bzl", "go_library", "go_binary")
load("@io_bazel_rules_go//proto:def.bzl", "go_proto_library")
load("@io_bazel_rules_go//proto:compiler.bzl", "go_proto_compiler")
load("@com_github_grpc_grpc//bazel:cc_grpc_library.bzl", "cc_grpc_library")
load("@com_github_grpc_grpc//bazel:python_rules.bzl", "py_grpc_library", "py_proto_library")
load("@grpc_ecosystem_grpc_gateway//protoc-gen-openapiv2:defs.bzl", "protoc_gen_openapiv2")
load("//tools:docgen.bzl", "gen_go_openapiv2", "protoc_gen_doc")

SERVICE_DEPS = [
    "@com_google_googleapis//google/rpc:status_proto",
]

ANNOTATION_DEPS = [
    "@com_google_googleapis//google/api:httpbody_proto",
    "@com_google_googleapis//google/api:client_proto",
    "@com_google_googleapis//google/api:annotations_proto",
]

WKT_DEPS = [
    "@com_google_protobuf//:duration_proto",
    "@com_google_protobuf//:timestamp_proto",
    "@com_google_protobuf//:empty_proto",
    "@com_google_protobuf//:field_mask_proto",
]

proto_library(
    name = "transcript_proto",
    srcs = ["transcript.proto"],
    visibility = ["//visibility:public"],
    deps = WKT_DEPS + ANNOTATION_DEPS + SERVICE_DEPS,
)

go_proto_library(
    name = "transcript_go_proto",
    compilers = [
        "@grpc_ecosystem_grpc_gateway//:go_grpc",
        "@grpc_ecosystem_grpc_gateway//:go_apiv2",
        "@grpc_ecosystem_grpc_gateway//protoc-gen-grpc-gateway:go_gen_grpc_gateway",
    ],
    importpath = "gitlab.com/tiro-is/tiro-apis-gogen/lr/transcript/v1alpha1",
    proto = ":transcript_proto",
    visibility = ["//visibility:public"],
    deps = [
        "@go_googleapis//google/api:annotations_go_proto",
        "@go_googleapis//google/api:httpbody_go_proto",
        "@go_googleapis//google/rpc:status_go_proto",
    ],
)

protoc_gen_openapiv2(
    name = "transcript_protoc_gen_openapiv2",
    proto = ":transcript_proto",
    # openapi_configuration = "",
    grpc_api_configuration = "//lr/transcript:transcript.yaml",
    single_output = True,  # Outputs a single swagger.json file.
)

gen_go_openapiv2(
    name = "transcript_go_openapiv2",
    package_name = "transcript",
    src_openapiv2 = ":transcript_protoc_gen_openapiv2",
)

go_library(
    name = "go_default_library",
    srcs = [
        ":transcript_go_openapiv2",
    ],
    embed = [
        ":transcript_go_proto",
    ],
    importpath = "gitlab.com/tiro-is/tiro-apis-gogen/lr/transcript/v1alpha1",
)

cc_proto_library(
    name = "transcript_cc_proto",
    deps = [":transcript_proto"],
)

cc_grpc_library(
    name = "transcript_cc_grpc",
    srcs = [":transcript_proto"],
    grpc_only = True,
    deps = [":transcript_cc_proto"],
)

py_proto_library(
    name = "transcript_py_proto",
    deps = [":transcript_proto"],
)

py_grpc_library(
    name = "transcript_py_grpc",
    srcs = [":transcript_proto"],
    deps = [":transcript_py_proto"],
)
