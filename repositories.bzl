load("@bazel_tools//tools/build_defs/repo:git.bzl", "new_git_repository")
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

def repositories():
    skylib_version = "1.4.2"
    skylib_sha256 = "66ffd9315665bfaafc96b52278f57c7e2dd09f5ede279ea6d39b2be471e7e3aa"
    http_archive(
        name = "bazel_skylib",
        sha256 = skylib_sha256,
        urls = [
            "https://github.com/bazelbuild/bazel-skylib/releases/download/{v}/bazel-skylib-{v}.tar.gz".format(v = skylib_version),
            "https://mirror.bazel.build/github.com/bazelbuild/bazel-skylib/releases/download/{v}/bazel-skylib-{v}.tar.gz".format(v = skylib_version),
        ],
    )

    rules_python_version = "0.32.2"
    rules_python_sha256 = "4912ced70dc1a2a8e4b86cec233b192ca053e82bc72d877b98e126156e8f228d"
    http_archive(
        name = "rules_python",
        url = "https://github.com/bazelbuild/rules_python/archive/{v}.tar.gz".format(v = rules_python_version),
        strip_prefix = "rules_python-{v}".format(v = rules_python_version),
        sha256 = rules_python_sha256,
    )

    io_bazel_rules_go_version = "0.34.0"
    http_archive(
        name = "io_bazel_rules_go",
        sha256 = "16e9fca53ed6bd4ff4ad76facc9b7b651a89db1689a2877d6fd7b82aa824e366",
        urls = [
            "https://mirror.bazel.build/github.com/bazelbuild/rules_go/releases/download/v{v}/rules_go-v{v}.zip".format(v = io_bazel_rules_go_version),
            "https://github.com/bazelbuild/rules_go/releases/download/v{v}/rules_go-v{v}.zip".format(v = io_bazel_rules_go_version),
        ],
    )

    bazel_gazelle_version = "0.22.2"
    http_archive(
        name = "bazel_gazelle",
        sha256 = "b85f48fa105c4403326e9525ad2b2cc437babaa6e15a3fc0b1dbab0ab064bc7c",
        urls = [
            "https://mirror.bazel.build/github.com/bazelbuild/bazel-gazelle/releases/download/v{v}/bazel-gazelle-v{v}.tar.gz".format(v = bazel_gazelle_version),
            "https://github.com/bazelbuild/bazel-gazelle/releases/download/v{v}/bazel-gazelle-v{v}.tar.gz".format(v = bazel_gazelle_version),
        ],
    )

    rules_pkg_version = "0.9.1"
    http_archive(
        name = "rules_pkg",
        urls = [
            "https://mirror.bazel.build/github.com/bazelbuild/rules_pkg/releases/download/{v}/rules_pkg-{v}.tar.gz".format(v = rules_pkg_version),
            "https://github.com/bazelbuild/rules_pkg/releases/download/{v}/rules_pkg-{v}.tar.gz".format(v = rules_pkg_version),
        ],
        sha256 = "8f9ee2dc10c1ae514ee599a8b42ed99fa262b757058f65ad3c384289ff70c4b8",
    )

    # gRPC
    grpc_version = "1.56.2"
    http_archive(
        name = "com_github_grpc_grpc",
        urls = ["https://github.com/grpc/grpc/archive/v{}.tar.gz".format(grpc_version)],
        sha256 = "931f07db9d48cff6a6007c1033ba6d691fe655bea2765444bc1ad974dfc840aa",
        strip_prefix = "grpc-{}".format(grpc_version),
    )

    googleapis_version = "c8bfd324b41ad1f6f65fed124572f92fe116517b"
    http_archive(
        name = "com_google_googleapis",
        urls = [
            "https://github.com/googleapis/googleapis/archive/{}.zip".format(googleapis_version),
        ],
        sha256 = "63a25daee9f828582dc9266ab3f050f81b7089ce096c8c584ce85548c71ce7f1",
        strip_prefix = "googleapis-{}".format(googleapis_version),
    )

    grpc_ecosystem_grpc_gateway_version = "2.1.0"
    http_archive(
        name = "grpc_ecosystem_grpc_gateway",
        sha256 = "2bca9747a3726497bd2cce43502858be936e23f13b6be292eb42bd422018e439",
        urls = [
            "https://github.com/grpc-ecosystem/grpc-gateway/archive/v{}.tar.gz".format(grpc_ecosystem_grpc_gateway_version),
        ],
        strip_prefix = "grpc-gateway-{}".format(grpc_ecosystem_grpc_gateway_version),
    )

    protoc_gen_doc_binary_version = "1.4.1"
    # NOTE: Only for Linux
    http_archive(
        name = "protoc_gen_doc_binary",
        urls = [
            "https://github.com/pseudomuto/protoc-gen-doc/releases/download/v{v}/protoc-gen-doc-{v}.linux-amd64.go1.15.2.tar.gz".format(v = protoc_gen_doc_binary_version),
        ],
        sha256 = "2e476c67063af55a5608f7ef876260eb4ca400b330b762a4f59096db501c5c8c",
        strip_prefix = "protoc-gen-doc-1.4.1.linux-amd64.go1.15.2",
        build_file = "@com_gitlab_tiro_is_tiro_apis//third_party:protoc_gen_doc.BUILD",
    )

def deploy_repositories():
    io_bazel_rules_docker_version = "0.23.0"
    io_bazel_rules_docker_sha256 = "85ffff62a4c22a74dbd98d05da6cf40f497344b3dbf1e1ab0a37ab2a1a6ca014"
    http_archive(
        name = "io_bazel_rules_docker",
        sha256 = io_bazel_rules_docker_sha256,
        strip_prefix = "rules_docker-{}".format(io_bazel_rules_docker_version),
        urls = [
            "https://github.com/bazelbuild/rules_docker/releases/download/v{v}/rules_docker-v{v}.tar.gz".format(v = io_bazel_rules_docker_version),
        ],
    )

    # Needed for k8s deployment
    rules_gitops_version = "3ee1ae1f6f4efdd1a57b7a60474e9f61eeeeb8b7"
    rules_gitops_sha256 = "3ac020eb39724c760d469b1a281f8a816d3f56e8874399218c4578e76b67cfed"
    http_archive(
        name = "com_adobe_rules_gitops",
        sha256 = rules_gitops_sha256,
        strip_prefix = "rules_gitops-{}".format(rules_gitops_version),
        urls = ["https://github.com/adobe/rules_gitops/archive/{}.zip".format(rules_gitops_version)],
    )
