import os
import pkg_resources
from setuptools import setup, find_packages, find_namespace_packages
from grpc_tools import protoc

package = 'tiro-apis'
version = '0.1.0'

well_known_proto_include = pkg_resources.resource_filename('grpc_tools',
                                                           '_proto')
local_proto_include = os.path.abspath(os.curdir)
print(well_known_proto_include)
print(local_proto_include)
if 0 != protoc.main([
        'noop',
        'lr/transcript/v1alpha1/transcript.proto',
        'lr/speech/v2beta1/speech.proto',
        'google/bytestream/bytestream.proto',
        '--mypy_out=./',
        '--python_out=./',
        '--grpc_python_out=./',
        '-I{}'.format(local_proto_include),
        '-I{}'.format(well_known_proto_include)]):
    raise Exception("Protobuf generation failure")

if 0 != protoc.main([
        'noop',
        'google/rpc/status.proto',
        'google/api/http.proto',
        'google/api/annotations.proto',
        '--mypy_out=./',
        '--python_out=./',
        '-I{}'.format(local_proto_include),
        '-I{}'.format(well_known_proto_include)]):
    raise Exception("Protobuf generation failure")


with open('requirements.txt') as req:
    install_requires = [line.strip() for line in req.readlines()]


packages = find_namespace_packages(exclude=['build', 'build.**', 'cmake',
                                            'test_package',
                                            'protoc-gen-swagger',
                                            'protoc-gen-swagger.**'])
print(packages)

setup(name=package,
      version=version,
      description='Python client libraries for Tiro APIs',
      url='https://tiro.is',
      author='Róbert Kjaran',
      author_email='robert@tiro.is',
      license='Apache 2.0',
      packages=packages,
      install_requires=install_requires,
      )
