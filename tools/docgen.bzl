load("@grpc_ecosystem_grpc_gateway//protoc-gen-openapiv2:defs.bzl", "protoc_gen_openapiv2")
load("@rules_proto//proto:defs.bzl", "ProtoInfo")

def gen_go_openapiv2(name, package_name, src_openapiv2, visibility=None):
    native.genrule(
        name = name,
        srcs = [src_openapiv2],
        outs = ["openapiv2.go"],
        cmd = """echo -e 'package {}\n' >$@
        echo -en 'var OpenAPIv2Data = []byte{{\n' >>$@
        od -w9999999999 -An -tx1 $< | sed 's/ \\([0-9a-z][0-9a-z]\\)/0x\\U\\1, /g' >>$@
        echo -en '}}\n\n'  >>$@
        """.format(package_name),
        visibility = visibility,
    )


# Based on: https://github.com/grpc-ecosystem/grpc-gateway/blob/ff93fab0103ee2a4f957c77ccf9d139305b19cdf/protoc-gen-openapiv2/defs.bzl
def _direct_source_infos(proto_info, provided_sources = []):
    """Returns sequence of `ProtoFileInfo` for `proto_info`'s direct sources.

    Files that are both in `proto_info`'s direct sources and in
    `provided_sources` are skipped. This is useful, e.g., for well-known
    protos that are already provided by the Protobuf runtime.

    Args:
      proto_info: An instance of `ProtoInfo`.
      provided_sources: Optional. A sequence of files to ignore.
          Usually, these files are already provided by the
          Protocol Buffer runtime (e.g. Well-Known protos).

    Returns: A sequence of `ProtoFileInfo` containing information about
        `proto_info`'s direct sources.
    """

    source_root = proto_info.proto_source_root
    if "." == source_root:
        return [struct(file = src, import_path = src.path) for src in proto_info.direct_sources]

    offset = len(source_root) + 1  # + '/'.

    infos = []
    for src in proto_info.direct_sources:
        # TODO(yannic): Remove this hack when we drop support for Bazel < 1.0.
        local_offset = offset
        if src.root.path and not source_root.startswith(src.root.path):
            # Before Bazel 1.0, `proto_source_root` wasn't guaranteed to be a
            # prefix of `src.path`. This could happend, e.g., if `file` was
            # generated (https://github.com/bazelbuild/bazel/issues/9215).
            local_offset += len(src.root.path) + 1  # + '/'.
        infos.append(struct(file = src, import_path = src.path[local_offset:]))

    return infos


def _run_proto_gen_doc(
        actions,
        proto_infos,
        target_name,
        service_name,
        template_file,
        transitive_proto_srcs,
        protoc,
        protoc_gen_doc):
    args = actions.args()

    args.add("--plugin", "protoc-gen-doc=%s" % protoc_gen_doc.path)

    for proto_info in proto_infos:
        args.add_all(proto_info.transitive_proto_path, format_each = "--proto_path=%s")

    doc_files = []

    doc_file = actions.declare_file("{}/.pages".format(target_name))
    actions.write(doc_file, "title: {}\ncollapse: false".format(service_name),
                  is_executable=False)
    doc_files.append(doc_file)

    for proto_file_info in proto_infos:
        file_name = "%s.md" % proto_file_info.direct_sources[0].basename[:-len(".proto")]
        doc_file = actions.declare_file("{}/{}".format(
            target_name, file_name))

        file_args = actions.args()

        file_args.add("--doc_out", doc_file.dirname)
        file_args.add("--doc_opt", "{},{}".format(template_file.path, doc_file.basename))

        file_args.add(proto_file_info.direct_sources[0].path)

        actions.run(
            executable = protoc,
            tools = [protoc_gen_doc],
            inputs = depset(
                direct = [template_file],
                transitive = [transitive_proto_srcs],
            ),
            outputs = [doc_file],
            arguments = [args, file_args],
        )
        doc_files.append(doc_file)

    return doc_files


def _proto_gen_doc_impl(ctx):
    proto_infos = [proto[ProtoInfo] for proto in ctx.attr.srcs]
    transitive_proto_srcs = []
    for p in proto_infos:
        transitive_proto_srcs.append(p.transitive_sources)

    return [
        DefaultInfo(
            files = depset(
                _run_proto_gen_doc(
                    actions = ctx.actions,
                    proto_infos = proto_infos,
                    target_name = ctx.attr.name,
                    service_name = ctx.attr.service_name,
                    template_file = ctx.file.template,
                    transitive_proto_srcs = depset(
                        direct = ctx.files._well_known_protos,
                        transitive = transitive_proto_srcs,
                    ),
                    protoc = ctx.executable._protoc,
                    protoc_gen_doc = ctx.executable._protoc_gen_doc,
                ),
            ),
        ),
    ]

protoc_gen_doc = rule(
    attrs = {
        "srcs": attr.label_list(
            allow_empty = False,
            mandatory = True,
            providers = [ProtoInfo],
        ),
        "template": attr.label(
            default = ":protoc_gen_doc.md.tmpl",
            allow_single_file = True,
        ),
        "service_name": attr.string(
            mandatory = True,
        ),
        "_protoc": attr.label(
            default = "@com_google_protobuf//:protoc",
            executable = True,
            cfg = "host",
        ),
        "_well_known_protos": attr.label(
            default = "@com_google_protobuf//:well_known_type_protos",
            allow_files = True,
        ),
        "_protoc_gen_doc": attr.label(
            default = Label("@protoc_gen_doc_binary//:protoc_gen_doc_plugin"),
            executable = True,
            cfg = "host",
        ),
    },
    implementation = _proto_gen_doc_impl,
)
